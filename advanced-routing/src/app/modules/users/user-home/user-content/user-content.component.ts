import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-user-content',
  templateUrl: './user-content.component.html',
  styleUrls: ['./user-content.component.scss'],
})
export class UserContentComponent implements OnInit {
  // constructor(private activatedRoute: ActivatedRoute) {}
  // ngOnInit(): void {
  //   this.activatedRoute.queryParams.subscribe((params) => {
  //     let selectedId = params['id'];
  //     console.log(selectedId); // Print the parameter to the console.
  //   });
  // }
  selectedId: number = 0;
  constructor(private route: ActivatedRoute) {}
  ngOnInit(): void {
    // this.selectedId = this.route.snapshot.params['id'];
    // console.log(this.selectedId);
    this.route.params.subscribe((params: Params) => {
      this.selectedId = params['id'];
      console.log(this.selectedId);
    });
  }
}
